package org.jetbrains.kotlinworkshop.introduction._1Intro

fun main(args: Array<String>) {
    val name = if (args.isNotEmpty()) args[0] else "Kotlin"
    println("Hello, $name!")

    // ${}
}

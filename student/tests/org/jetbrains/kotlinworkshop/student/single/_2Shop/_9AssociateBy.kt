package org.jetbrains.kotlinworkshop.student.single._2Shop

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class _9AssociateBy {
    @Test
    fun testAssociateCustomersByName() {
        assertEquals(customers, shop.associateCustomersByName())
    }
}
